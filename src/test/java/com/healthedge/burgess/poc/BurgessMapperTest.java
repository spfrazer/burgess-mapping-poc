/*
 * Copyright © 2001-2019 HealthEdge Software, Inc. All Rights Reserved.
 *
 * This software is proprietary information of HealthEdge Software, Inc.
 * and may not be reproduced or redistributed for any purpose.
 */

package com.healthedge.burgess.poc;

import codehaus.jackson.map.ObjectMapper;
import com.healthedge.entityprovider.ConsolidatedClaimEntityProvider;
import com.healthedge.iom.IOMFactory;
import com.healthedge.iom.generated.beans.ConsolidatedClaim;
import com.healthedge.iom.generated.beans.GrouperPricerParameters;
import com.healthedge.iom.generated.metadata.Types;
import com.healthedge.burgess.poc.wrappers.MsDrgGrouperResponseWrapper;
import com.healthedge.burgess.poc.wrappers.ResponseMessage;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpStatus;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.Test;

import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.sql.Date;
import java.util.stream.Collectors;

import static com.healthedge.util.DateConstants.JAN_1_2001;
import static org.junit.Assert.assertEquals;

public class BurgessMapperTest {

    @Test
    public void testMap() throws IOException, AuthenticationException {
        System.setProperty("SDPF_HCC_UNIQUE_IDENTIFIER", "PHIC");
        BurgessMapper burgessMapper = new BurgessMapper();

        long claimId;
        //claimId = 1l; // professional
        //claimId = 2007l; //institutional
        claimId = 2010l; // inpatient LTC

        // parameters required for fetching DRG reference
        GrouperPricerParameters params = null;
        Date admissionDate = JAN_1_2001;

        // fetch the claim
        ConsolidatedClaim claim = new ConsolidatedClaimEntityProvider(
                IOMFactory.createReference(Types.ConsolidatedClaim, claimId));


        // map the claim to JSON request format
        String request = burgessMapper.map(claim);

        request = prettyPrint(request);
        System.out.println("Request: " + request);

        // make the HTTP request to Burgess
        String response = makeRequest(
                "https://aetnapreprod-service.burgesssource.com/process",
                "6c50a165B0754AE890c9AC9195d9c859",
                "",
                request);

        System.out.println(prettyPrint(response));

        // map the response to a result object
        MsDrgGrouperResponseWrapper responseWrapper = burgessMapper.mapResponse(params, admissionDate, response);

        assertEquals("Response status", "Success", responseWrapper.getOptionalReturnCode());

        System.out.println(responseWrapper.toXML());
        System.out.println(responseWrapper.getAllMessages().stream().map(ResponseMessage::toString).collect(Collectors.joining(",")));
    }

    private String makeRequest(String url, String user, String password, String requestBody) throws IOException, AuthenticationException {
        try (CloseableHttpClient client = HttpClients.createDefault()) {
            HttpPost httpPost = new HttpPost(url);
            UsernamePasswordCredentials creds = new UsernamePasswordCredentials(user, password);
            httpPost.addHeader(new BasicScheme().authenticate(creds, httpPost, null));

            httpPost.setEntity(new StringEntity(requestBody));
            httpPost.setHeader("Accept", MediaType.APPLICATION_JSON);
            httpPost.setHeader("Content-type", MediaType.APPLICATION_JSON);

            try (CloseableHttpResponse httpResponse = client.execute(httpPost)) {
                System.out.println(httpResponse.getStatusLine());

                if (httpResponse.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                    throw new IOException("Request failed: " + httpResponse.getStatusLine().getStatusCode() + " " + httpResponse.getStatusLine().getReasonPhrase());
                }

                // success
                return IOUtils.toString(httpResponse.getEntity().getContent());
            }
        }
    }

    private String prettyPrint(String json) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Object resultObj = mapper.readValue(json, Object.class);
        return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(resultObj);
    }
}
/*
 * Copyright © 2001-2019 HealthEdge Software, Inc. All Rights Reserved.
 *
 * This software is proprietary information of HealthEdge Software, Inc.
 * and may not be reproduced or redistributed for any purpose.
 */

package com.healthedge.burgess.poc;

import com.healthedge.adjudication.ServiceDateContext;
import com.healthedge.adjudication.domain.model.IMember;
import com.healthedge.adjudication.domain.model.MatchedMember;
import com.healthedge.burgess.poc.wrappers.ClaimWrapper;
import com.healthedge.burgess.poc.wrappers.LineWrapper;
import com.healthedge.burgess.poc.wrappers.SupplierWrapper;
import com.healthedge.entityprovider.ClaimLineEntityProvider;
import com.healthedge.entityprovider.MembershipEntityProvider;
import com.healthedge.entityprovider.SupplierLocationEntityProvider;
import com.healthedge.iom.IOMAggregate;
import com.healthedge.iom.cache.FetchContext;
import com.healthedge.iom.cache.IOMFetcherException;
import com.healthedge.iom.generated.beans.*;
import com.healthedge.tools.metadata.RawXMLParser;
import com.healthedge.util.DateUtil;
import com.healthedge.util.NestedRuntimeException;
import org.apache.commons.io.IOUtils;

import java.util.ArrayList;
import java.util.List;

public class MockClaimWrapper extends ClaimWrapper {
    private final String supplierXml;
    private final String locationXml;
    private final String memberXml;
    private final String drgXml;

    private String supplierTin;
    private String primaryClassificationCode;
    private String primaryClassificationName;


    public MockClaimWrapper(String mockFilePath) {
        super(new ConsolidatedClaim(read(mockFilePath + "/" + "ConsolidatedClaim.xml")), null);
        supplierXml = mockFilePath + "/" + "Supplier.xml";
        locationXml = mockFilePath + "/" + "SupplierLocation.xml";
        memberXml = mockFilePath + "/" + "Membership.xml";
        drgXml = mockFilePath + "/" + "DRG.xml";
    }

    @Override
    public SupplierWrapper getSupplier() {
        return new SupplierWrapper(read(supplierXml)) {
            @Override
            public String getTIN() {
                return supplierTin;
            }

            @Override
            public ProviderTaxonomy getPrimaryClassification() {
                ProviderTaxonomy taxonomy = new ProviderTaxonomy();
                taxonomy.setCode(primaryClassificationCode);
                taxonomy.setCodeName(primaryClassificationName);
                return taxonomy;
            }
        };
    }

    @Override
    public SupplierLocationEntityProvider getSupplierLocationEntityProvider() {
        return new SupplierLocationEntityProvider(read(locationXml));
    }

    @Override
    public synchronized ConsolidatedInput getOptionalConsolidatedInput() {
        return new ConsolidatedInput(super.getOptionalConsolidatedInput().getAggregate(), FetchContext.getFetchContextForNow()) {
            @Override
            public DRG fetchOptionalCalculatedDrgCode() throws IOMFetcherException {
                if (getOptionalCalculatedDrgCode() != null) {
                    return new DRG(read(drgXml));
                }
                return null;
            }
        };
    }

    @Override
    public IMember getMember() {
        return new MatchedMember(new MembershipEntityProvider(read(memberXml), FetchContext.getFetchContextForNow()));
    }

    @Override
    public List<ClaimLineEntityProvider> getClaimLines() {
        List<ClaimLineEntityProvider> lineList = new ArrayList<>(getDeliveredServiceList().size());
        int count = 1;
        for (TransformedDeliveredService line : getDeliveredServiceList()) {
            if (line.getIsReplaced()) {
                continue;
            }
            LineWrapper lineEntity = new LineWrapper(line, count) {
                @Override
                public ServiceDateContext getServiceContext() {
                    return new ServiceDateContext(DateUtil.getCurrentDate(), DateUtil.getCurrentTime());
                }
            };
            lineEntity.setClaim(this);
            lineList.add(lineEntity);
            count++;
        }
        return lineList;
    }

    public static IOMAggregate read(String xmlPath) {
        try {
            String xml = IOUtils.toString(MockClaimWrapper.class.getResourceAsStream(xmlPath));
            return RawXMLParser.parseXML(xml, false);
        } catch(Exception e) {
            throw new NestedRuntimeException("Error while reading from XML", e);
        }
    }

    public String getSupplierTin() {
        return supplierTin;
    }

    public void setSupplierTin(String supplierTin) {
        this.supplierTin = supplierTin;
    }

    public String getPrimaryClassificationCode() {
        return primaryClassificationCode;
    }

    public void setPrimaryClassificationCode(String primaryClassificationCode) {
        this.primaryClassificationCode = primaryClassificationCode;
    }

    public String getPrimaryClassificationName() {
        return primaryClassificationName;
    }

    public void setPrimaryClassificationName(String primaryClassificationName) {
        this.primaryClassificationName = primaryClassificationName;
    }
}

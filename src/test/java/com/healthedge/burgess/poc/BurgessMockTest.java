/*
 * Copyright © 2001-2019 HealthEdge Software, Inc. All Rights Reserved.
 *
 * This software is proprietary information of HealthEdge Software, Inc.
 * and may not be reproduced or redistributed for any purpose.
 */

package com.healthedge.burgess.poc;

import com.healthedge.adjudication.ServiceDateContext;
import com.healthedge.adjudication.domain.model.IMember;
import com.healthedge.adjudication.domain.model.MatchedMember;
import com.healthedge.burgess.poc.wrappers.ClaimWrapper;
import com.healthedge.burgess.poc.wrappers.LineWrapper;
import com.healthedge.burgess.poc.wrappers.SupplierWrapper;
import com.healthedge.entityprovider.ClaimLineEntityProvider;
import com.healthedge.entityprovider.MembershipEntityProvider;
import com.healthedge.entityprovider.SupplierLocationEntityProvider;
import com.healthedge.entityprovider.TestCodeEntryCacheResource;
import com.healthedge.iom.IOMAggregate;
import com.healthedge.iom.IOMReference;
import com.healthedge.iom.cache.FetchContext;
import com.healthedge.iom.cache.IOMFetcherException;
import com.healthedge.iom.generated.beans.*;
import com.healthedge.iom.generated.metadata.CodeSetNameDomainValues;
import com.healthedge.iom.generated.metadata.IdentificationTypeDomainValues;
import com.healthedge.iom.generated.metadata.IndividualAddressTypeDomainValues;
import com.healthedge.tools.metadata.RawXMLParser;
import com.healthedge.util.DateUtil;
import com.healthedge.util.NestedRuntimeException;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class BurgessMockTest {

    @Rule
    public TestCodeEntryCacheResource codeEntryCache = new TestCodeEntryCacheResource();

    @Before
    public void init() {
        codeEntryCache.addCodeEntry(2554L, CodeSetNameDomainValues.CLAIM_ADMISSION_SOURCES, "2554", "Non-Health Care Facility Point of Origin");
        codeEntryCache.addCodeEntry(2563L, CodeSetNameDomainValues.CLAIM_ADMISSION_SOURCES, "2563", "A Reserved");

        codeEntryCache.addCodeEntry(1L, IdentificationTypeDomainValues.DOMAIN_NAME, IdentificationTypeDomainValues.MEDICARE_PROVIDER_NUMBER, "Medicare Provider Number");
        codeEntryCache.addCodeEntry(2L, IdentificationTypeDomainValues.DOMAIN_NAME, IdentificationTypeDomainValues.FEDERAL_TAXPAYER_ID, "Federal Tax Identifier");
        codeEntryCache.addCodeEntry(3L, IndividualAddressTypeDomainValues.DOMAIN_NAME, IndividualAddressTypeDomainValues.RESIDENTIAL, "Residential");
    }

    @Test
    public void testLTCExample() throws Exception {
        MockClaimWrapper claim = new MockClaimWrapper("/LTC");
        claim.setSupplierTin("37-7402799");
        claim.setPrimaryClassificationCode("HO_0000000");
        claim.setPrimaryClassificationName("HO-Acute Short Term Hospital");

        BurgessMapper mapper = new BurgessMapper();

        String request = mapper.map(claim);
        System.out.println(request);
    }

    @Test
    public void testHHAExample() throws Exception {
        MockClaimWrapper claim = new MockClaimWrapper("/HHA");
        claim.setSupplierTin("70-4138368");
        claim.setPrimaryClassificationCode("HA_0000000");
        claim.setPrimaryClassificationName("HA-Home Health Care Agency");

        BurgessMapper mapper = new BurgessMapper();

        String request = mapper.map(claim);
        System.out.println(request);
    }

    public IOMAggregate read(String xmlPath) {
        try {
            String xml = IOUtils.toString(this.getClass().getResourceAsStream(xmlPath));
            return RawXMLParser.parseXML(xml, false);
        } catch(Exception e) {
            throw new NestedRuntimeException("Error while reading from XML", e);
        }
    }
}

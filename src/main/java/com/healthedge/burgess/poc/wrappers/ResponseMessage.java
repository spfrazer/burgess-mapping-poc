/*
 * Copyright © 2001-2019 HealthEdge Software, Inc. All Rights Reserved.
 *
 * This software is proprietary information of HealthEdge Software, Inc.
 * and may not be reproduced or redistributed for any purpose.
 */

package com.healthedge.burgess.poc.wrappers;

public class ResponseMessage {
    private String disposition;
    private String editId;
    private String messageText;
    private String adjustmentType;
    private String adjustmentValue;
    private String mappedSystemMessage;

    public String getDisposition() {
        return disposition;
    }

    public void setDisposition(String disposition) {
        this.disposition = disposition;
    }

    public String getEditId() {
        return editId;
    }

    public void setEditId(String editId) {
        this.editId = editId;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public String getAdjustmentType() {
        return adjustmentType;
    }

    public void setAdjustmentType(String adjustmentType) {
        this.adjustmentType = adjustmentType;
    }

    public String getAdjustmentValue() {
        return adjustmentValue;
    }

    public void setAdjustmentValue(String adjustmentValue) {
        this.adjustmentValue = adjustmentValue;
    }

    public String getMappedSystemMessage() {
        return mappedSystemMessage;
    }

    public void setMappedSystemMessage(String mappedSystemMessage) {
        this.mappedSystemMessage = mappedSystemMessage;
    }

    @Override
    public String toString() {
        return "ResponseMessage{" +
                "disposition='" + disposition + '\'' +
                ", editId='" + editId + '\'' +
                ", messageText='" + messageText + '\'' +
                ", adjustmentType='" + adjustmentType + '\'' +
                ", adjustmentValue='" + adjustmentValue + '\'' +
                ", mappedSystemMessage='" + mappedSystemMessage + '\'' +
                '}';
    }
}

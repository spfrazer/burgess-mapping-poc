/*
 * Copyright © 2001-2019 HealthEdge Software, Inc. All Rights Reserved.
 *
 * This software is proprietary information of HealthEdge Software, Inc.
 * and may not be reproduced or redistributed for any purpose.
 */

package com.healthedge.burgess.poc.wrappers;

import com.healthedge.entityprovider.SupplierEntityProvider;
import com.healthedge.iom.IOMAggregate;
import com.healthedge.iom.IOMReference;
import com.healthedge.iom.IOMWrapperBean;
import com.healthedge.iom.cache.FetchContext;
import com.healthedge.iom.generated.beans.ProviderTaxonomy;
import com.healthedge.iom.generated.beans.Supplier;

import java.util.List;

public class SupplierWrapper extends SupplierEntityProvider {
    public SupplierWrapper(Supplier supplier, FetchContext fc) {
        super(supplier, fc);
    }

    public SupplierWrapper(IOMAggregate aggr) {
        super(aggr);
    }

    public ProviderTaxonomy getPrimaryClassification() {
        List<IOMReference> refs = getSupplierClassType().asList();
        if (!refs.isEmpty()) {
            return (ProviderTaxonomy)IOMWrapperBean.getImmutableBean(refs.get(0), getFetchContext());
        }
        return null;
    }

    public String getTIN() {
        return getTIN(getFetchContext());
    }
}

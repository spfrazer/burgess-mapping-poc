/*
 * Copyright © 2001-2019 HealthEdge Software, Inc. All Rights Reserved.
 *
 * This software is proprietary information of HealthEdge Software, Inc.
 * and may not be reproduced or redistributed for any purpose.
 */

package com.healthedge.burgess.poc.wrappers;

import com.healthedge.entityprovider.ClaimLineEntityProvider;
import com.healthedge.entityprovider.CodeEntryEntityProvider;
import com.healthedge.entityprovider.ConsolidatedClaimEntityProvider;
import com.healthedge.entityprovider.SupplierEntityProvider;
import com.healthedge.iom.IOMReference;
import com.healthedge.iom.IOMWrapperBean;
import com.healthedge.iom.generated.beans.*;
import com.healthedge.iom.generated.metadata.DiagnosisTypeCodeDomainValues;
import com.healthedge.iom.generated.metadata.IdentificationTypeDomainValues;

import java.util.ArrayList;
import java.util.List;

public class ClaimWrapper extends ConsolidatedClaimEntityProvider {
    private GrouperPricerParameters grouperPricerParameters;

    public ClaimWrapper(IOMReference claimRef, GrouperPricerParameters parameters) {
        super(claimRef);
        this.grouperPricerParameters = parameters;
    }

    public ClaimWrapper(ConsolidatedClaim claim, GrouperPricerParameters parameters) {
        super(claim);
        this.grouperPricerParameters = parameters;
    }

    @Override
    public SupplierWrapper getSupplier() {
        SupplierEntityProvider supplier = super.getSupplier();
        return supplier != null ? new SupplierWrapper(supplier, supplier.getFetchContext()) : null;
    }

    @Override
    public List<ClaimLineEntityProvider> getClaimLines() {
        List<ClaimLineEntityProvider> lineList = new ArrayList<>(getDeliveredServiceList().size());
        int count = 1;
        for (TransformedDeliveredService line : getDeliveredServiceList()) {
            if (line.getIsReplaced()) {
                continue;
            }
            LineWrapper lineEntity = new LineWrapper(line, count);
            lineEntity.setClaim(this);
            lineList.add(lineEntity);
            count++;
        }
        return lineList;
    }

    public String getSupplierId() {
        IOMReference idType = getSupplierIdType();
        if (idType == null) {
            idType = CodeEntryEntityProvider
                    .getCodeEntryRef(IdentificationTypeDomainValues.DOMAIN_NAME,
                            IdentificationTypeDomainValues.MEDICARE_PROVIDER_NUMBER,
                            getFetchContext().getEndorsementDate());
        }
        return idType != null ? getSupplierId(idType) : null;
    }

    public String getDiagnosisTypeCode() {
        DiagnosisInformation diagnosisInfo = getDiagnosis1();
        if (diagnosisInfo != null) {
            Diagnosis diagnosis = diagnosisInfo.fetchOptionalDiagnosis();
            if (diagnosis != null &&
                    (DiagnosisTypeCodeDomainValues.ICD_9.equals(diagnosis.getDiagnosisTypeCode())
                    || (DiagnosisTypeCodeDomainValues.ICD_10.equals(diagnosis.getDiagnosisTypeCode())))) {
                return diagnosis.getDiagnosisTypeCode();
            }
        }
        return null;
    }


    public Condition getConditionCode1() {
        return getConditionCodeAtIndex(0);
    }
    public Condition getConditionCode2() {
        return getConditionCodeAtIndex(1);
    }
    public Condition getConditionCode3() {
        return getConditionCodeAtIndex(2);
    }
    public Condition getConditionCode4() {
        return getConditionCodeAtIndex(3);
    }
    public Condition getConditionCode5() {
        return getConditionCodeAtIndex(4);
    }
    public Condition getConditionCode6() {
        return getConditionCodeAtIndex(5);
    }
    public Condition getConditionCode7() {
        return getConditionCodeAtIndex(6);
    }
    public Condition getConditionCode8() {
        return getConditionCodeAtIndex(7);
    }
    public Condition getConditionCode9() {
        return getConditionCodeAtIndex(8);
    }
    public Condition getConditionCode10() {
        return getConditionCodeAtIndex(10);
    }
    public Condition getConditionCode11() {
        return getConditionCodeAtIndex(11);
    }

    private Condition getConditionCodeAtIndex(int index) {
        List<IOMReference> codes = getConditionCodes();
        if (codes.size() > index) {
            return (Condition) IOMWrapperBean.getImmutableBean(codes.get(index), getFetchContext());
        }
        return null;
    }

    public DiagnosisInformation getDiagnosis1() { return getDiagnosisAtIndex(0); }
    public DiagnosisInformation getDiagnosis2() { return getDiagnosisAtIndex(1); }
    public DiagnosisInformation getDiagnosis3() { return getDiagnosisAtIndex(2); }
    public DiagnosisInformation getDiagnosis4() { return getDiagnosisAtIndex(3); }
    public DiagnosisInformation getDiagnosis5() { return getDiagnosisAtIndex(4); }
    public DiagnosisInformation getDiagnosis6() { return getDiagnosisAtIndex(5); }
    public DiagnosisInformation getDiagnosis7() { return getDiagnosisAtIndex(6); }
    public DiagnosisInformation getDiagnosis8() { return getDiagnosisAtIndex(7); }
    public DiagnosisInformation getDiagnosis9() { return getDiagnosisAtIndex(8); }
    public DiagnosisInformation getDiagnosis10() { return getDiagnosisAtIndex(9); }
    public DiagnosisInformation getDiagnosis11() { return getDiagnosisAtIndex(10); }
    public DiagnosisInformation getDiagnosis12() { return getDiagnosisAtIndex(11); }
    public DiagnosisInformation getDiagnosis13() { return getDiagnosisAtIndex(12); }
    public DiagnosisInformation getDiagnosis14() { return getDiagnosisAtIndex(13); }
    public DiagnosisInformation getDiagnosis15() { return getDiagnosisAtIndex(14); }
    public DiagnosisInformation getDiagnosis16() { return getDiagnosisAtIndex(15); }
    public DiagnosisInformation getDiagnosis17() { return getDiagnosisAtIndex(16); }
    public DiagnosisInformation getDiagnosis18() { return getDiagnosisAtIndex(17); }
    public DiagnosisInformation getDiagnosis19() { return getDiagnosisAtIndex(18); }
    public DiagnosisInformation getDiagnosis20() { return getDiagnosisAtIndex(19); }
    public DiagnosisInformation getDiagnosis21() { return getDiagnosisAtIndex(20); }
    public DiagnosisInformation getDiagnosis22() { return getDiagnosisAtIndex(21); }
    public DiagnosisInformation getDiagnosis23() { return getDiagnosisAtIndex(22); }
    public DiagnosisInformation getDiagnosis24() { return getDiagnosisAtIndex(23); }
    public DiagnosisInformation getDiagnosis25() { return getDiagnosisAtIndex(24); }

    private DiagnosisInformation getDiagnosisAtIndex(int index) {
        List<DiagnosisInformation> diags = new ArrayList<>();
        DiagnosisInformation principalDiagnosis = getOptionalConsolidatedInput().getOptionalPrincipalDiagnosis();
        if (principalDiagnosis != null) {
            diags.add(principalDiagnosis);
        }
        diags.addAll(getOtherDiagnosisList());
        if (index < diags.size()) {
            return diags.get(index);
        }
        return null;
    }

    public ProcedureInfo getProcedure1() { return getProcedureAtIndex(0); }
    public ProcedureInfo getProcedure2() { return getProcedureAtIndex(1); }
    public ProcedureInfo getProcedure3() { return getProcedureAtIndex(2); }
    public ProcedureInfo getProcedure4() { return getProcedureAtIndex(3); }
    public ProcedureInfo getProcedure5() { return getProcedureAtIndex(4); }
    public ProcedureInfo getProcedure6() { return getProcedureAtIndex(5); }
    public ProcedureInfo getProcedure7() { return getProcedureAtIndex(6); }
    public ProcedureInfo getProcedure8() { return getProcedureAtIndex(7); }
    public ProcedureInfo getProcedure9() { return getProcedureAtIndex(8); }
    public ProcedureInfo getProcedure10() { return getProcedureAtIndex(9); }
    public ProcedureInfo getProcedure11() { return getProcedureAtIndex(10); }
    public ProcedureInfo getProcedure12() { return getProcedureAtIndex(11); }
    public ProcedureInfo getProcedure13() { return getProcedureAtIndex(12); }
    public ProcedureInfo getProcedure14() { return getProcedureAtIndex(13); }
    public ProcedureInfo getProcedure15() { return getProcedureAtIndex(14); }
    public ProcedureInfo getProcedure16() { return getProcedureAtIndex(15); }
    public ProcedureInfo getProcedure17() { return getProcedureAtIndex(16); }
    public ProcedureInfo getProcedure18() { return getProcedureAtIndex(17); }
    public ProcedureInfo getProcedure19() { return getProcedureAtIndex(18); }
    public ProcedureInfo getProcedure20() { return getProcedureAtIndex(19); }
    public ProcedureInfo getProcedure21() { return getProcedureAtIndex(20); }
    public ProcedureInfo getProcedure22() { return getProcedureAtIndex(21); }
    public ProcedureInfo getProcedure23() { return getProcedureAtIndex(22); }
    public ProcedureInfo getProcedure24() { return getProcedureAtIndex(23); }
    public ProcedureInfo getProcedure25() { return getProcedureAtIndex(24); }
    
    private ProcedureInfo getProcedureAtIndex(int index) {
        List<ProcedureInfo> procs = new ArrayList<>();
        ProcedureInfo principalProc = getOptionalConsolidatedInput().getOptionalPrincipalProcedureInfo();
        if (principalProc != null) {
            procs.add(principalProc);
        }
        procs.addAll(getOptionalConsolidatedInput().getOtherProcedureInfo().asList());
        if (index < procs.size()) {
            return procs.get(index);
        }
        return null;
    }


    public Diagnosis getReasonForVisit1() { return getReasonForVisitAtIndex(0); }
    public Diagnosis getReasonForVisit2() { return getReasonForVisitAtIndex(1); }
    public Diagnosis getReasonForVisit3() { return getReasonForVisitAtIndex(2); }

    private Diagnosis getReasonForVisitAtIndex(int index) {
        List<Diagnosis> diags = new ArrayList<>();
        for (IOMReference ref : getOptionalConsolidatedInput().getReasonForVisitDiagnosisList()) {
            diags.add((Diagnosis) IOMWrapperBean.getImmutableBean(ref, getFetchContext()));
        }
        if (index < diags.size()) {
            return diags.get(index);
        }
        return null;
    }

    public PaymentRequestValueCode getValueCode1() {
        return getValueCodeAtIndex(0);
    }
    public PaymentRequestValueCode getValueCode2() {
        return getValueCodeAtIndex(1);
    }
    public PaymentRequestValueCode getValueCode3() {
        return getValueCodeAtIndex(2);
    }
    public PaymentRequestValueCode getValueCode4() {
        return getValueCodeAtIndex(3);
    }
    public PaymentRequestValueCode getValueCode5() {
        return getValueCodeAtIndex(4);
    }
    public PaymentRequestValueCode getValueCode6() {
        return getValueCodeAtIndex(5);
    }
    public PaymentRequestValueCode getValueCode7() {
        return getValueCodeAtIndex(6);
    }
    public PaymentRequestValueCode getValueCode8() {
        return getValueCodeAtIndex(7);
    }
    public PaymentRequestValueCode getValueCode9() {
        return getValueCodeAtIndex(8);
    }
    public PaymentRequestValueCode getValueCode10() {
        return getValueCodeAtIndex(9);
    }
    public PaymentRequestValueCode getValueCode11() {
        return getValueCodeAtIndex(10);
    }
    public PaymentRequestValueCode getValueCode12() {
        return getValueCodeAtIndex(11);
    }

    private PaymentRequestValueCode getValueCodeAtIndex(int index) {
        List<PaymentRequestValueCode> codes = getOptionalConsolidatedInput().getPrValueCodeList().asList();
        if (index < codes.size()) {
            return codes.get(index);
        }
        return null;
    }

    public OccurrenceCode getOccurrenceCode1() {
        return getOccurrenceCodeAtIndex(0);
    }

    public OccurrenceCode getOccurrenceCode2() {
        return getOccurrenceCodeAtIndex(1);
    }

    public OccurrenceCode getOccurrenceCode3() {
        return getOccurrenceCodeAtIndex(2);
    }

    public OccurrenceCode getOccurrenceCode4() {
        return getOccurrenceCodeAtIndex(3);
    }

    public OccurrenceCode getOccurrenceCode5() {
        return getOccurrenceCodeAtIndex(4);
    }

    public OccurrenceCode getOccurrenceCode6() {
        return getOccurrenceCodeAtIndex(5);
    }

    public OccurrenceCode getOccurrenceCode7() {
        return getOccurrenceCodeAtIndex(6);
    }

    public OccurrenceCode getOccurrenceCode8() {
        return getOccurrenceCodeAtIndex(7);
    }

    public OccurrenceCode getOccurrenceCode9() {
        return getOccurrenceCodeAtIndex(8);
    }

    public OccurrenceCode getOccurrenceCode10() {
        return getOccurrenceCodeAtIndex(9);
    }

    public OccurrenceCode getOccurrenceCode11() {
        return getOccurrenceCodeAtIndex(10);
    }

    public OccurrenceCode getOccurrenceCode12() {
        return getOccurrenceCodeAtIndex(11);
    }

    private OccurrenceCode getOccurrenceCodeAtIndex(int index) {
        List<OccurrenceCode> codes = getOptionalConsolidatedInput().getPrOccurrenceCode().asList();
        if (index < codes.size()) {
            return codes.get(index);
        }
        return null;
    }

    public OccurrenceCode getOccurrenceSpan1() {
        return getOccurrenceSpanAtIndex(0);
    }

    public OccurrenceCode getOccurrenceSpan2() {
        return getOccurrenceSpanAtIndex(1);
    }

    public OccurrenceCode getOccurrenceSpan3() {
        return getOccurrenceSpanAtIndex(2);
    }

    public OccurrenceCode getOccurrenceSpan4() {
        return getOccurrenceSpanAtIndex(3);
    }

    public OccurrenceCode getOccurrenceSpan5() {
        return getOccurrenceSpanAtIndex(4);
    }

    public OccurrenceCode getOccurrenceSpan6() {
        return getOccurrenceSpanAtIndex(5);
    }

    public OccurrenceCode getOccurrenceSpan7() {
        return getOccurrenceSpanAtIndex(6);
    }

    public OccurrenceCode getOccurrenceSpan8() {
        return getOccurrenceSpanAtIndex(7);
    }

    public OccurrenceCode getOccurrenceSpan9() {
        return getOccurrenceSpanAtIndex(8);
    }

    public OccurrenceCode getOccurrenceSpan10() {
        return getOccurrenceSpanAtIndex(9);
    }

    public OccurrenceCode getOccurrenceSpan11() {
        return getOccurrenceSpanAtIndex(10);
    }

    public OccurrenceCode getOccurrenceSpan12() {
        return getOccurrenceSpanAtIndex(11);
    }

    private OccurrenceCode getOccurrenceSpanAtIndex(int index) {
        List<OccurrenceCode> codes = getOptionalConsolidatedInput().getPrOccurrenceSpan().asList();
        if (index < codes.size()) {
            return codes.get(index);
        }
        return null;
    }

    public IOMReference getSupplierIdType() {
        return grouperPricerParameters != null? grouperPricerParameters.getOptionalSupplierIdType() : null;
    }
}

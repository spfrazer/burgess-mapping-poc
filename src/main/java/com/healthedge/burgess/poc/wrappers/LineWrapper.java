/*
 * Copyright © 2001-2019 HealthEdge Software, Inc. All Rights Reserved.
 *
 * This software is proprietary information of HealthEdge Software, Inc.
 * and may not be reproduced or redistributed for any purpose.
 */

package com.healthedge.burgess.poc.wrappers;

import com.healthedge.entityprovider.ClaimLineEntityProvider;
import com.healthedge.iom.IOMReference;
import com.healthedge.iom.IOMWrapperBean;
import com.healthedge.iom.generated.beans.DiagnosisInformation;
import com.healthedge.iom.generated.beans.Modifier;
import com.healthedge.iom.generated.beans.NDCCode;
import com.healthedge.iom.generated.beans.TransformedDeliveredService;

import java.util.List;

public class LineWrapper extends ClaimLineEntityProvider {
    private int lineIndex;

    public LineWrapper(TransformedDeliveredService tds, int index) {
        super(tds);
        this.lineIndex = index;
    }

    public NDCCode getPrimaryNDCCode() {
        List<NDCCode> codes = getNdcCodesInfo();
        if (!codes.isEmpty()) {
            return codes.get(0);
        }
        return null;
    }

    public int getLineIndex() {
        return lineIndex;
    }

    public Modifier getModifier1() {
        return getModifierAtIndex(0);
    }
    public Modifier getModifier2() {
        return getModifierAtIndex(1);
    }
    public Modifier getModifier3() {
        return getModifierAtIndex(2);
    }
    public Modifier getModifier4() {
        return getModifierAtIndex(3);
    }
    public Modifier getModifier5() {
        return getModifierAtIndex(4);
    }

    private Modifier getModifierAtIndex(int index) {
        List<IOMReference> modifiers = getModifiers();
        if (modifiers.size() > index) {
            return (Modifier) IOMWrapperBean.getImmutableBean(modifiers.get(index), getFetchContext());
        }
        return null;
    }

    public DiagnosisInformation getOtherDiagnosis1() {
        return getOtherDiagnosisAtIndex(0);
    }
    public DiagnosisInformation getOtherDiagnosis2() {
        return getOtherDiagnosisAtIndex(1);
    }
    public DiagnosisInformation getOtherDiagnosis3() {
        return getOtherDiagnosisAtIndex(2);
    }

    private DiagnosisInformation getOtherDiagnosisAtIndex(int index) {
        List<DiagnosisInformation> otherDiag = getClaim().getOtherDiagnosisList();
        if (index < otherDiag.size()) {
            return otherDiag.get(index);
        }
        return null;
    }
}

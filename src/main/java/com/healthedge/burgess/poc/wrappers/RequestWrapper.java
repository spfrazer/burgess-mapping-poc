/*
 * Copyright © 2001-2019 HealthEdge Software, Inc. All Rights Reserved.
 *
 * This software is proprietary information of HealthEdge Software, Inc.
 * and may not be reproduced or redistributed for any purpose.
 */

package com.healthedge.burgess.poc.wrappers;

import com.healthedge.iom.generated.beans.GrouperPricerParameters;

public class RequestWrapper {
    private ClaimWrapper claim;
    private GrouperPricerParameters grouperPricerParameters;

    public RequestWrapper(ClaimWrapper claim) {
        this.claim = claim;
    }

    public ClaimWrapper getClaim() {
        return claim;
    }

    public void setClaim(ClaimWrapper claim) {
        this.claim = claim;
    }

    public GrouperPricerParameters getGrouperPricerParameters() {
        return grouperPricerParameters;
    }

    public void setGrouperPricerParameters(GrouperPricerParameters grouperPricerParameters) {
        this.grouperPricerParameters = grouperPricerParameters;
    }
}

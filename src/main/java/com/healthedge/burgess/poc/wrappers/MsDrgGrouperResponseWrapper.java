/*
 * Copyright © 2001-2019 HealthEdge Software, Inc. All Rights Reserved.
 *
 * This software is proprietary information of HealthEdge Software, Inc.
 * and may not be reproduced or redistributed for any purpose.
 */

package com.healthedge.burgess.poc.wrappers;

import com.healthedge.adjudication.externalsystem.grouperpricer.inpatient.DRGResolver;
import com.healthedge.iom.generated.beans.EasyGroupMsDrgGrouperClaimResult;
import com.healthedge.iom.generated.beans.GrouperPricerParameters;
import com.healthedge.iom.generated.metadata.DRGTypeDomainValues;

import java.sql.Date;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MsDrgGrouperResponseWrapper extends EasyGroupMsDrgGrouperClaimResult {
    private static final Set<String> DRG_EDIT_CODES = new HashSet<>(Arrays.asList("001905"));

    private String status;
    private String statusMessage;
    private List<ResponseMessage> allMessages;

    public void updateDrg(GrouperPricerParameters params, Date admissionDate) {
        // search messages for a list of well-known DRG codes
        for (ResponseMessage message : allMessages) {
            if (DRG_EDIT_CODES.contains(message.getEditId())) {
                String drgCode = message.getAdjustmentValue();
                setDrgCode(drgCode);
                String drgType = params != null ? params.getOptionalDrgType() : null;
                if (drgType == null) {
                    drgType = DRGTypeDomainValues.MS_DRG;
                }
                setDrg(DRGResolver.getDRG(drgType, drgCode, admissionDate));
            }
        }

        this.setGrouperPricer(params);
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public List<ResponseMessage> getAllMessages() {
        return allMessages;
    }

    public void setAllMessages(List<ResponseMessage> allMessages) {
        this.allMessages = allMessages;
    }
}

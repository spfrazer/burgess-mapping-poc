/*
 * Copyright © 2001-2019 HealthEdge Software, Inc. All Rights Reserved.
 *
 * This software is proprietary information of HealthEdge Software, Inc.
 * and may not be reproduced or redistributed for any purpose.
 */

package com.healthedge.burgess.poc.typehandlers;

import org.beanio.types.TypeConversionException;
import org.beanio.types.TypeHandler;

public class TrimStringTypeHandler implements TypeHandler {
    private String trimRegex = "";
    private String replaceString = "";

    @Override
    public Object parse(String text) throws TypeConversionException {
        return text;
    }

    @Override
    public String format(Object value) {
        if (value instanceof String) {
            String inputString = (String)value;
            return inputString.replaceAll(trimRegex, replaceString);
        }
        return null;
    }

    @Override
    public Class<?> getType() {
        return String.class;
    }

    public String getTrimRegex() {
        return trimRegex;
    }

    public void setTrimRegex(String trimRegex) {
        this.trimRegex = trimRegex;
    }

    public String getReplaceString() {
        return replaceString;
    }

    public void setReplaceString(String replaceString) {
        this.replaceString = replaceString;
    }
}
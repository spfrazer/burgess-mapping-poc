/*
 * Copyright © 2001-2019 HealthEdge Software, Inc. All Rights Reserved.
 *
 * This software is proprietary information of HealthEdge Software, Inc.
 * and may not be reproduced or redistributed for any purpose.
 */

package com.healthedge.burgess.poc.typehandlers;

import org.beanio.types.TypeConversionException;
import org.beanio.types.TypeHandler;

/**
 * Boolean type handler for BurgessMapper, allowing end users to override
 * mappings of True and False values.
 *
 * The String representation of True can be configured via the setTrueString() method,
 * and the String representation of False can be configured via the setFalseString() method.
 */
public class BooleanTypeHandler implements TypeHandler {
    private String trueString = Boolean.TRUE.toString();
    private String falseString = Boolean.FALSE.toString();

    @Override
    public Object parse(String s) throws TypeConversionException {
        return s != null && s.equals(trueString);
    }

    @Override
    public String format(Object o) {
        if (o instanceof Boolean) {
            return (Boolean)o ? trueString : falseString;
        }
        return "";
    }

    @Override
    public Class<?> getType() {
        return Boolean.class;
    }

    /**
     * Configured String representation of the True boolean value.
     *
     * @return String representation
     */
    public String getTrueString() {
        return trueString;
    }

    /**
     * Set the String representation of the True boolean value.
     *
     * @param trueString String representation
     */
    public void setTrueString(String trueString) {
        this.trueString = trueString;
    }

    /**
     * Configured String representation of the False boolean value.
     *
     * @return String representation
     */
    public String getFalseString() {
        return falseString;
    }

    /**
     * Set the String representation of the False boolean value.
     *
     * @param falseString String representation
     */
    public void setFalseString(String falseString) {
        this.falseString = falseString;
    }
}
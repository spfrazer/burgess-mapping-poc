/*
 * Copyright © 2001-2019 HealthEdge Software, Inc. All Rights Reserved.
 *
 * This software is proprietary information of HealthEdge Software, Inc.
 * and may not be reproduced or redistributed for any purpose.
 */

package com.healthedge.burgess.poc.typehandlers;

import org.beanio.types.ConfigurableTypeHandler;
import org.beanio.types.TypeConversionException;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;

/**
 * Type handler that can crosswalk Strings "inline" using a crosswalk
 * defined through a String property value rather than an external file.
 * Can be used for simple mappings with only a few possible values.
 */
public class InlineCrosswalkTypeHandler implements ConfigurableTypeHandler {
    private static final String DELIMITER = ",";
    private static final String FIELD_DELIMITER = "=";

    private static final int EXPECTED_FIELDS = 2;

    private String crosswalk = "";

    @Override
    public Object parse(String s) throws TypeConversionException {
        return getParseMap().get(s);
    }

    @Override
    public String format(Object o) {
        if (o instanceof String) {
            return getFormatMap().get(o);
        }
        return null;
    }

    private Map<String,String> getFormatMap() {
        Map<String,String> ret = new HashMap<>();

        StringTokenizer fieldTokenizer = new StringTokenizer(crosswalk, DELIMITER);
        while (fieldTokenizer.hasMoreTokens()) {
            String field = fieldTokenizer.nextToken();
            String[] values = field.split(FIELD_DELIMITER);
            if (values.length == EXPECTED_FIELDS) {
                ret.put(values[0], values[1]);
            }
        }
        return ret;
    }


    private Map<String,String> getParseMap() {
        Map<String,String> ret = new HashMap<>();

        StringTokenizer fieldTokenizer = new StringTokenizer(crosswalk, DELIMITER);
        while (fieldTokenizer.hasMoreTokens()) {
            String field = fieldTokenizer.nextToken();
            String[] values = field.split(FIELD_DELIMITER);
            if (values.length == EXPECTED_FIELDS) {
                ret.put(values[1], values[0]);
            }
        }
        return ret;
    }

    @Override
    public Class<?> getType() {
        return String.class;
    }


    /*
     * (non-Javadoc)
     * @see org.beanio.types.AbstractDateTypeHandler#newInstance(java.util.Properties)
     */
    public InlineCrosswalkTypeHandler newInstance(Properties properties) {
        String pattern = properties.getProperty(FORMAT_SETTING);
        if (pattern == null || "".equals(pattern)) {
            return this;
        }
        if (pattern.equals(getCrosswalk())) {
            return this;
        }

        InlineCrosswalkTypeHandler handler = new InlineCrosswalkTypeHandler();
        handler.setCrosswalk(pattern);
        return handler;
    }

    public String getCrosswalk() {
        return crosswalk;
    }

    public void setCrosswalk(String crosswalk) {
        this.crosswalk = crosswalk;
    }
}
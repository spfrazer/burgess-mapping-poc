/*
 * Copyright © 2001-2019 HealthEdge Software, Inc. All Rights Reserved.
 *
 * This software is proprietary information of HealthEdge Software, Inc.
 * and may not be reproduced or redistributed for any purpose.
 */

package com.healthedge.burgess.poc.typehandlers;

import com.healthedge.iom.IOMReference;
import org.beanio.types.TypeConversionException;
import org.beanio.types.TypeHandler;

public class IOMReferenceTypeHandler implements TypeHandler {

    @Override
    public Object parse(String text) throws TypeConversionException {
        return null;
    }

    @Override
    public String format(Object value) {
        if (value instanceof IOMReference) {
            return ((IOMReference)value).getLeafId().toString();
        }
        return null;
    }

    @Override
    public Class<?> getType() {
        return IOMReference.class;
    }
}

/*
 * Copyright © 2001-2019 HealthEdge Software, Inc. All Rights Reserved.
 *
 * This software is proprietary information of HealthEdge Software, Inc.
 * and may not be reproduced or redistributed for any purpose.
 */

package com.healthedge.burgess.poc.typehandlers;

import org.beanio.types.TypeConversionException;
import org.beanio.types.TypeHandler;

/**
 * Type handler to convert numbers to their Alphabet equievlants (ex. 0 = A, 1= B, etc.)
 */
public class NumberAlphabetTypeHandler implements TypeHandler {

    @Override
    public Object parse(String text) throws TypeConversionException {
        return null;
    }

    @Override
    public String format(Object value) {
        if (value instanceof Integer) {
            Integer intValue = (Integer)value;
            if (intValue >= 0 && intValue < 26) {
                return Character.toString((char)('A' + intValue));
            }

        }
        return null;
    }

    @Override
    public Class<?> getType() {
        return Integer.class;
    }
}

/*
 * Copyright © 2001-2019 HealthEdge Software, Inc. All Rights Reserved.
 *
 * This software is proprietary information of HealthEdge Software, Inc.
 * and may not be reproduced or redistributed for any purpose.
 */

package com.healthedge.burgess.poc.typehandlers;

import org.beanio.types.DateTypeHandlerSupport;

import java.sql.Date;
import java.util.Properties;

public class SqlDateTypeHandler extends org.beanio.types.DateTypeHandler {
    public SqlDateTypeHandler() {

    }

    public SqlDateTypeHandler(String pattern) {
        super(pattern);
    }

    @Override
    public Class<?> getType() {
        return Date.class;
    }

    /*
     * (non-Javadoc)
     * @see org.beanio.types.AbstractDateTypeHandler#newInstance(java.util.Properties)
     */
    @Override
    public DateTypeHandlerSupport newInstance(Properties properties) {
        String pattern = properties.getProperty(FORMAT_SETTING);
        if (pattern == null || "".equals(pattern)) {
            return this;
        }
        if (pattern.equals(getPattern())) {
            return this;
        }

        DateTypeHandlerSupport handler = new SqlDateTypeHandler(pattern);
        handler.setPattern(pattern);
        return handler;
    }
}

/*
 * Copyright © 2001-2019 HealthEdge Software, Inc. All Rights Reserved.
 *
 * This software is proprietary information of HealthEdge Software, Inc.
 * and may not be reproduced or redistributed for any purpose.
 */

package com.healthedge.burgess.poc;

import com.healthedge.burgess.poc.util.JSONUtil;
import com.healthedge.cvc.impl.claim.service.ClaimServiceHelper;
import com.healthedge.entityprovider.ConsolidatedClaimEntityProvider;
import org.apache.commons.cli.*;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Command line utility to fetch a ConsolidatedClaim by HCC claim number,
 * map it to Burgess JSON format, and send the request to Burgess.
 *
 * To run:
 *  -Build this project and copy burgess-mapping-poc jar to Weblogic domain under jars/
 *  -Copy following jars to Weblogic domain under tools/third-party/lib/
 *      ~/.m2/repository/org/apache/httpcomponents/httpclient/4.5.6/httpclient-4.5.6.jar
 *      ~/.m2/repository/org/apache/httpcomponents/httpcore/4.4.10/httpcore-4.4.10.jar
 *      ~/.m2/repository/org/beanio/beanio/2.1.0/beanio-2.1.0.jar
 *
 *  Example usage (where 915184102 is the HCC claim number):
 *  execJava com.healthedge.burgess.poc.BurgessTest -url https://aetnapreprod-service.burgesssource.com/process -user 6c50a165B0754AE890c9AC9195d9c859 915184102
 */
public class BurgessTest {
    private BurgessMapper burgessMapper = new BurgessMapper();
    private BurgessClient burgessClient = new BurgessClient();
    private boolean prettyPrint = true;

    public void testClaim(ConsolidatedClaimEntityProvider claim) throws Exception {
        String hccClaimNumber = claim.getHccClaimNumber();

        // do the request mapping
        String requestStr = burgessMapper.map(claim);

        // print the request
        requestStr = JSONUtil.format(requestStr, prettyPrint);
        System.out.println("==== Request start claim  " + hccClaimNumber + " ====");
        System.out.println(requestStr);
        System.out.println("==== Request end claim  " + hccClaimNumber + " ====");


        if (StringUtils.isBlank(burgessClient.getUrl())) {
            System.out.println("Burgess URL not set, skipping request");
            return;
        }

        System.out.println("Sending request to " + burgessClient.getUrl());

        // send the request to Burgess
        String responseStr = burgessClient.makeRequest(requestStr);

        // print the response
        responseStr = JSONUtil.format(responseStr, prettyPrint);
        System.out.println("==== Response start claim  " + hccClaimNumber + " ====");
        System.out.println(responseStr);
        System.out.println("==== Response end claim  " + hccClaimNumber + " ====");
        System.out.println();
    }

    public BurgessMapper getBurgessMapper() {
        return burgessMapper;
    }

    public void setBurgessMapper(BurgessMapper burgessMapper) {
        this.burgessMapper = burgessMapper;
    }

    public BurgessClient getBurgessClient() {
        return burgessClient;
    }

    public void setBurgessClient(BurgessClient burgessClient) {
        this.burgessClient = burgessClient;
    }

    public boolean isPrettyPrint() {
        return prettyPrint;
    }

    public void setPrettyPrint(boolean prettyPrint) {
        this.prettyPrint = prettyPrint;
    }

    public static void main(String[] args) {
        Options options = new Options();
        options.addOption("url", true, "Burgess URL");
        options.addOption("user", true, "Burgess User name");
        options.addOption("password", true, "Burgess password");
        options.addOption("noformat", false, "Don't \"pretty print\" the JSON request and response");
        options.addOption("h", false, "Help");

        BurgessTest testObj = new BurgessTest();
        List<String> claimIds = new ArrayList<>();

        CommandLineParser parser = new BasicParser();
        try {
            CommandLine cmd = parser.parse(options, args);
            if (cmd.hasOption("h")) {
                new HelpFormatter().printHelp("BurgessTest", options);
                System.exit(0);
            }
            if (cmd.hasOption("noformat")) {
                testObj.setPrettyPrint(false);
            }
            testObj.getBurgessClient().setUrl(cmd.getOptionValue("url"));
            testObj.getBurgessClient().setUser(cmd.getOptionValue("user"));
            testObj.getBurgessClient().setPassword(cmd.getOptionValue("password"));
            claimIds.addAll(cmd.getArgList());
        } catch (ParseException e) {
            e.printStackTrace();
            System.exit(1);
        }

        for (String claimId : claimIds) {
            ConsolidatedClaimEntityProvider claim = ClaimServiceHelper.getLatestClaim(claimId);
            try {
                testObj.testClaim(claim);
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
    }
}

/*
 * Copyright © 2001-2019 HealthEdge Software, Inc. All Rights Reserved.
 *
 * This software is proprietary information of HealthEdge Software, Inc.
 * and may not be reproduced or redistributed for any purpose.
 */

package com.healthedge.burgess.poc;

import com.healthedge.iom.generated.beans.ConsolidatedClaim;
import com.healthedge.iom.generated.beans.GrouperPricerParameters;
import com.healthedge.burgess.poc.wrappers.ClaimWrapper;
import com.healthedge.burgess.poc.wrappers.MsDrgGrouperResponseWrapper;
import com.healthedge.burgess.poc.wrappers.RequestWrapper;
import com.healthedge.util.DateUtil;
import org.beanio.BeanReader;
import org.beanio.BeanWriter;
import org.beanio.StreamFactory;

import java.io.StringReader;
import java.io.StringWriter;
import java.sql.Date;

public class BurgessMapper {
    public String map(ConsolidatedClaim inputClaim) {
        ClaimWrapper claim = new ClaimWrapper(inputClaim, null);
        return map(claim);
    }

    public String map(ClaimWrapper claim) {
        RequestWrapper request = new RequestWrapper(claim);

        StreamFactory factory = StreamFactory.newInstance(this.getClass().getClassLoader());

        factory.loadResource("burgess-outbound.xml");

        StringWriter writer = new StringWriter();
        BeanWriter out = factory.createWriter("ClaimDetailsStream", writer);
        out.write(request);

        return  writer.toString();
    }

    public MsDrgGrouperResponseWrapper mapResponse(GrouperPricerParameters params, Date admissionDate, String response) {

        StreamFactory factory = StreamFactory.newInstance(this.getClass().getClassLoader());

        factory.loadResource("burgess-inbound.xml");

        StringReader reader = new StringReader(response);
        BeanReader in = factory.createReader("ClaimDetailsStream", reader);
        MsDrgGrouperResponseWrapper ret = (MsDrgGrouperResponseWrapper) in.read();

        // do any post processing
        ret.updateDrg(params, admissionDate);
        ret.setResponseDate(DateUtil.getCurrentTime());
        return ret;
    }

}

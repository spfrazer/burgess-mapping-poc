/*
 * Copyright © 2001-2019 HealthEdge Software, Inc. All Rights Reserved.
 *
 * This software is proprietary information of HealthEdge Software, Inc.
 * and may not be reproduced or redistributed for any purpose.
 */

package com.healthedge.burgess.poc;

import com.healthedge.util.StringUtil;
import org.apache.http.HttpStatus;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.http.util.EntityUtils;

import javax.ws.rs.core.MediaType;
import java.io.IOException;

public class BurgessClient {
    private String url;
    private String user;
    private String password;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String makeRequest(String requestBody) throws IOException, AuthenticationException {
        if (url == null) {
            throw new RuntimeException("URL not set");
        }
        try (CloseableHttpClient client = HttpClients.custom().setRedirectStrategy(new LaxRedirectStrategy()).build()) {
            HttpPost httpPost = new HttpPost(url);
            if (user != null || password != null) {
                UsernamePasswordCredentials creds = new UsernamePasswordCredentials(
                        StringUtil.emptyIfNull(user, true),
                        StringUtil.emptyIfNull(password, true));
                httpPost.addHeader(new BasicScheme().authenticate(creds, httpPost, null));
            }

            httpPost.setEntity(new StringEntity(requestBody));
            httpPost.setHeader("Accept", MediaType.APPLICATION_JSON);
            httpPost.setHeader("Content-type", MediaType.APPLICATION_JSON);

            try (CloseableHttpResponse httpResponse = client.execute(httpPost)) {
                System.out.println(httpResponse.getStatusLine());

                if (httpResponse.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                    throw new IOException("Request failed: " + httpResponse.getStatusLine().getStatusCode() + " " + httpResponse.getStatusLine().getReasonPhrase());
                }

                // success
                return EntityUtils.toString(httpResponse.getEntity());
            }
        }
    }

}

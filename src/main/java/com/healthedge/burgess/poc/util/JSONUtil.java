/*
 * Copyright © 2001-2019 HealthEdge Software, Inc. All Rights Reserved.
 *
 * This software is proprietary information of HealthEdge Software, Inc.
 * and may not be reproduced or redistributed for any purpose.
 */

package com.healthedge.burgess.poc.util;

import codehaus.jackson.map.ObjectMapper;

import java.io.IOException;

public class JSONUtil {
    private JSONUtil() { }

    public static String format(String json, boolean prettyPrint) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Object resultObj = mapper.readValue(json, Object.class);
        if (prettyPrint) {
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(resultObj);
        } else {
            return mapper.writeValueAsString(resultObj);
        }
    }
}

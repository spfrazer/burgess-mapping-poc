# burgess-mapping-poc

Private repository for Burgess POC work that was done for PAYOR-64744.  The mapping uses BeanIO to map a Payor ConsolidatedClaim to the Burgess JSON request format, with a few thin Java wrapper classes.